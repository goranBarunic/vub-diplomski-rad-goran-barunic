create or replace function f_timediff(
   ts1   in timestamp,
   ts2   in timestamp,
   units in varchar2)
/* units - ms=millisecond ss=second, mi=minute, hr=hour, dy=day */
return number is 

diff interval day(9) to second(9) := ts1 - ts2;

BEGIN
   return (
      extract(day from diff)*24*60*60 +
      extract(hour from diff)*60*60 +
      extract(minute from diff)*60 +
      extract(second from diff) ) /
      case (lower(units))
         when 'ms' then 1/1000
         when 'ss' then 1
         when 'mi' then 60
         when 'hr' then 60*60
         when 'dy' then 24*60*60
      else null
      end;
END;

set serveroutput on
declare
   ts1 timestamp := systimestamp;
   rezultat number;
begin
   select f_timediff(systimestamp,ts1, 'ms') into rezultat from dual;
   dbms_output.put_line(rezultat);
end;




