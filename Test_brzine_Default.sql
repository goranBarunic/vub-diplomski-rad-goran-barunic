CREATE OR REPLACE DIRECTORY CTEST AS 'C:\test';
GRANT READ ON DIRECTORY CTEST TO PUBLIC;
/
-------------------------------------------------------------------------------------
set serveroutput on
declare
    ts1 timestamp := systimestamp;
    rezultat number;
    pb clob ;
    pbj clob ;
    l_counter NUMBER := 0;
    ispis_sql VARCHAR2(30000) := '';
    ispis_JSON VARCHAR2(30000) := '';
    header VARCHAR2(30000) := '';
    
    --broj izvršavanja testova
    max_counter NUMBER := 10;
   
    out_File  UTL_FILE.FILE_TYPE;
   
begin
    out_File := UTL_FILE.FOPEN('CTEST', 'mjerenja.csv' , 'W');
    
    dbms_output.put_line( '****** STARTING TEST! ****** ' );
    
    LOOP     
        l_counter := l_counter + 1;
        ts1 := systimestamp;
        IF l_counter > max_counter THEN
        EXIT;
        END IF;
        dbms_output.put_line( '****** TEST ' || l_counter || ' ****** ' );
    --sql test
        select na.mjesto 
        into pb
        from NASELJA na
        where na.mjesto = 'Crikvenica';
    --sql test end    
        select f_timediff(systimestamp,ts1, 'ms') into rezultat from dual;
        ispis_sql := ispis_sql || TO_CHAR(rezultat, '9999') || ';';
        dbms_output.put_line('SQL: ' || rezultat || ' Miliseconds');
          
        ts1 := systimestamp;
    --json test    
        select /*+ INDEX(NASELJA_JSON,j_naselje)*/ jn.j_naselje
        into pbj
        from NASELJA_JSON jn, 
            json_table(j_naselje, '$'
                COLUMNS(mjesta varchar2 path '$.NASELJE.MJESTO')) js
        where js.mjesta = 'Zvijerci';
    --json test end   
        select f_timediff(systimestamp,ts1, 'ms') into rezultat from dual;
        ispis_JSON := ispis_JSON || TO_CHAR(rezultat, '9999') || ';';
        header := header || 'Result ' || l_counter || ';' ;
        dbms_output.put_line('JSON: ' || rezultat || ' Miliseconds');
    END LOOP;
  
    dbms_output.put_line( '****** TEST ENDED! ******' );
    dbms_output.put_line( 'SQL results: ' || ispis_sql );
    dbms_output.put_line( 'JSON results: ' || ispis_JSON);
    
    --ispis u file rezultata
    UTL_FILE.PUT_LINE(out_file, 'SQL results: ');
    UTL_FILE.PUT_LINE(out_file, header);
    UTL_FILE.PUT_LINE(out_file ,  ispis_sql);
    UTL_FILE.PUT_LINE(out_file, 'JSON results: ');
    UTL_FILE.PUT_LINE(out_file, header);
    UTL_FILE.PUT_LINE(out_file , ispis_JSON);
    UTL_FILE.FCLOSE(out_file);
end;
/
-------------------------------------------------------------------------------------
