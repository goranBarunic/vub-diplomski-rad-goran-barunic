set serveroutput on
DECLARE
  vodeci_broj_banke NUMBER := 2402006; 
  vrsta_racuna NUMBER := 1; namjena_racuna NUMBER := 1; k_id NUMBER;
  
  kontrolna_znamenka NUMBER := 0;
  br1 NUMBER := 0; br2 NUMBER := 0; br3 NUMBER := 0; br4 NUMBER := 0; br5 NUMBER := 0; br6 NUMBER := 0; br7 NUMBER := 0; br8 NUMBER := 0; br9 NUMBER := 0;   
 
  korisnicki_racun number;
 
  CURSOR c_korisnik
  IS
    SELECT id,kontolni_br_rac FROM korisnici where kontolni_br_rac = 0 
    for update of kontolni_br_rac;

BEGIN
  FOR kor_rec IN c_korisnik
  LOOP
    k_id := kor_rec.id + 1000000;
    kontrolna_znamenka := vrsta_racuna||namjena_racuna||k_id;
    
    br1 := trunc(mod((kontrolna_znamenka / 100000000),10), 0);
    br2 := trunc(mod((kontrolna_znamenka / 10000000),10),0);
    br3 := trunc(mod((kontrolna_znamenka / 1000000),10),0);
    br4 := trunc(mod((kontrolna_znamenka / 100000),10),0);
    br5 := trunc(mod((kontrolna_znamenka / 10000),10),0);
    br6 := trunc(mod((kontrolna_znamenka / 1000),10),0);
    br7 := trunc(mod((kontrolna_znamenka / 100),10),0);
    br8 := trunc(mod((kontrolna_znamenka / 10),10),0);
    br9 := trunc(mod(kontrolna_znamenka,10),0);

    
    kontrolna_znamenka := mod((br1 + 10),10);    
    if kontrolna_znamenka = 0 then
    kontrolna_znamenka := 10;
    kontrolna_znamenka := mod((kontrolna_znamenka * 2),11);
    else
    kontrolna_znamenka := mod((kontrolna_znamenka * 2),11);
    end if;
    
    kontrolna_znamenka := mod((br2 + kontrolna_znamenka),10);
    if kontrolna_znamenka = 0 then
    kontrolna_znamenka := 10;
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    else
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    end if;

    kontrolna_znamenka := mod((br3 + kontrolna_znamenka),10);
    if kontrolna_znamenka = 0 then
    kontrolna_znamenka := 10;
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    else
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    end if;

    kontrolna_znamenka := mod((br4 + kontrolna_znamenka),10);
    if kontrolna_znamenka = 0 then
    kontrolna_znamenka := 10;
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    else
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    end if;
    
    kontrolna_znamenka := mod((br5 + kontrolna_znamenka),10);
    if kontrolna_znamenka = 0 then
    kontrolna_znamenka := 10;
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    else
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    end if;
    
    kontrolna_znamenka := mod((br6 + kontrolna_znamenka),10);
    if kontrolna_znamenka = 0 then
    kontrolna_znamenka := 10;
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    else
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    end if;
    
    kontrolna_znamenka := mod((br7 + kontrolna_znamenka),10);
    if kontrolna_znamenka = 0 then
    kontrolna_znamenka := 10;
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    else
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    end if;
    
    kontrolna_znamenka := mod((br8 + kontrolna_znamenka),10);
    if kontrolna_znamenka = 0 then
    kontrolna_znamenka := 10;
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    else
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    end if;
    
    kontrolna_znamenka := mod((br9 + kontrolna_znamenka),10);
    if kontrolna_znamenka = 0 then
    kontrolna_znamenka := 10;
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    else
    kontrolna_znamenka := mod((kontrolna_znamenka * 2 ),11);
    end if;
    
    kontrolna_znamenka :=  11 - kontrolna_znamenka;
 
    korisnicki_racun := vodeci_broj_banke||vrsta_racuna||namjena_racuna||k_id||kontrolna_znamenka;

    DBMS_OUTPUT.PUT_LINE('Broj racuna u banci: ' || korisnicki_racun);
    
    update korisnici
    set kontolni_br_rac = korisnicki_racun
    where current of c_korisnik;   
  
  END LOOP;
  
  commit;
END;

