--------------------------------------------------------
--  DDL for Table STANJA
--------------------------------------------------------

  CREATE TABLE "STANJA" 
   (	
    "ID" NUMBER(9,0), 
    "BROJPARTIJE" NUMBER(18,0),
    "VAZIDO" DATE,
    "IDKORISNIKA" NUMBER(9,0),
    "IZNOSHRK" NUMBER(20,0),
    "IZNOSEUR" NUMBER(20,0),
    "UPDATED" TIMESTAMP (6),
    "CREATED" TIMESTAMP (6)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
  NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

--------------------------------------------------------
--INSERTING into STANJA
--------------------------------------------------------

set serveroutput on
DECLARE
    CURSOR c_korisnik
    IS
    Select 
        ID, 
        KONTOLNI_BR_RAC 
    from 
        KORISNICI;
BEGIN
    FOR r_korisnik IN c_korisnik
    LOOP
    INSERT INTO STANJA 
        (ID, 
        BROJPARTIJE, 
        VAZIDO, 
        IDKORISNIKA, 
        IZNOSHRK, 
        IZNOSEUR, 
        UPDATED, 
        CREATED) 
    VALUES
        (r_korisnik.ID+10, 
        r_korisnik.KONTOLNI_BR_RAC, 
        to_timestamp('2050-01-04','YYYY-MM-DD'), 
        r_korisnik.ID, 
        dbms_random.value(100.00,10000.00), 
        dbms_random.value(100.00,10000.00), 
        to_timestamp('2020-01-06 10:55:40,150145000','YYYY-MM-DD HH24:MI:SSXFF'),
        to_timestamp('2019-11-26 12:55:06,959306000','YYYY-MM-DD HH24:MI:SSXFF'));
    END LOOP;
END;

--------------------------------------------------------
--  DDL for Trigger TRIG_STANJA_BI
--------------------------------------------------------

CREATE SEQUENCE  "FAKS"."SEQ_STANJA_ID"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;

  CREATE OR REPLACE NONEDITIONABLE TRIGGER "TRIG_STANJA_BI" 
BEFORE INSERT ON STANJA
 FOR EACH ROW
  WHEN (new.ID IS NULL) BEGIN
 SELECT
 SEQ_STANJA_ID.NEXTVAL
 INTO
 :new.ID
 FROM dual;

 SELECT
 SYSTIMESTAMP
 INTO
 :new.UPDATED
 FROM dual;

 SELECT
 SYSTIMESTAMP
 INTO
 :new.CREATED
 FROM dual;
END;
/
ALTER TRIGGER "TRIG_STANJA_BI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRIG_STANJA_AU
--------------------------------------------------------

CREATE OR REPLACE NONEDITIONABLE TRIGGER "TRIG_STANJA_AU" 
BEFORE UPDATE ON STANJA
REFERENCING NEW AS NEWER OLD AS
OLDER
FOR EACH ROW
BEGIN
 :NEWER.UPDATED := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "TRIG_STANJA_AU" ENABLE;

--------------------------------------------------------
--  Constraints for Table STANJA
--------------------------------------------------------

  ALTER TABLE "STANJA" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "STANJA" MODIFY ("BROJPARTIJE" NOT NULL ENABLE);
  ALTER TABLE "STANJA" MODIFY ("VAZIDO" NOT NULL ENABLE);
  ALTER TABLE "STANJA" MODIFY ("IDKORISNIKA" NOT NULL ENABLE);
  ALTER TABLE "STANJA" MODIFY ("IZNOSHRK" NOT NULL ENABLE);
  ALTER TABLE "STANJA" MODIFY ("IZNOSEUR" NOT NULL ENABLE);
  ALTER TABLE "STANJA" MODIFY ("UPDATED" NOT NULL ENABLE);
  ALTER TABLE "STANJA" MODIFY ("CREATED" NOT NULL ENABLE); 
  ALTER TABLE "STANJA" ADD CONSTRAINT "STANJA_PK" PRIMARY KEY ("ID");
  