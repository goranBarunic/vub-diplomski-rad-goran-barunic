--------------------------------------------------------
--  DDL for Table ZUPANIJE
--------------------------------------------------------

  CREATE TABLE "ZUPANIJE"
   (	"ID" NUMBER(2,0), 
	"ZUPANIJA" VARCHAR2(26 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into ZUPANIJE
SET DEFINE OFF;
Insert into ZUPANIJE (ID,ZUPANIJA) values ('1','GRAD ZAGREB');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('2','ZAGREBAÈKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('3','DUBROVAÈKO-NERETVANSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('4','SPLITSKO-DALMATINSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('5','ŠIBENSKO-KNINSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('6','ZADARSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('7','OSJEÈKO-BARANJSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('8','VUKOVARSKO-SRIJEMSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('9','VIROVITIÈKO-PODRAVSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('10','POŽEŠKO-SLAVONSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('11','BRODSKO-POSAVSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('12','MEÐIMURSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('13','VARAŽDINSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('14','BJELOVARSKO-BILOGORSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('15','SISAÈKO-MOSLAVAÈKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('16','KARLOVAÈKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('17','KOPRIVNIÈKO-KRIŽEVAÈKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('18','KRAPINSKO-ZAGORSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('19','PRIMORSKO-GORANSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('20','ISTARSKA');
Insert into ZUPANIJE (ID,ZUPANIJA) values ('21','LIÈKO-SENJSKA');
--------------------------------------------------------
--  DDL for Index ZUPANIJE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ZUPANIJE_PK" ON "ZUPANIJE" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table ZUPANIJE
--------------------------------------------------------

  ALTER TABLE "ZUPANIJE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "ZUPANIJE" MODIFY ("ZUPANIJA" NOT NULL ENABLE);
  ALTER TABLE "ZUPANIJE" ADD CONSTRAINT "ZUPANIJE_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;