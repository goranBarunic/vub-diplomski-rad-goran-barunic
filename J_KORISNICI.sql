--kreacija tablice
CREATE TABLE "J_KORISNICI" 
    (  
    "ID" NUMBER(9,0), 
    "IME" VARCHAR2(20 BYTE), 
	"PREZIME" VARCHAR2(20 BYTE), 
	"EMAIL" VARCHAR2(40 BYTE), 
	"PASSWORD" VARCHAR2(20 BYTE), 
	"OVLASTI" NUMBER(1,0), 
	"SPOL" NUMBER(1,0), 
    "UPDATED" TIMESTAMP (6),
    "CREATED" TIMESTAMP (6),
    "GODISTE" NUMBER(4,0), 
	"OIB" NUMBER(11,0), 
	"CHGPASS" NUMBER(1,0) DEFAULT 1, 
	"DELETED" NUMBER(1,0) DEFAULT 0,
    kontrolni_br_rac NUMBER(18,0) DEFAULT 0,
    "NASELJA_JSON" clob,
    "KONTAKT_JSON" clob,   
    "STANJA_JSON" clob,
    CONSTRAINT ensure_json_nas CHECK (NASELJA_JSON IS JSON),
    CONSTRAINT ensure_json_kon CHECK (KONTAKT_JSON IS JSON),
    CONSTRAINT ensure_json_sta CHECK (STANJA_JSON IS JSON)
    );
/
--------------------------------------------------------------------------------------------------------------------------
--punjenje tablice
set serveroutput on
DECLARE
    CURSOR c_korisnici IS   
        select
            kor.ID,
            kor.IME,
            kor.PREZIME,
            kor.EMAIL,
            kor.PASSWORD,
            kor.OVLASTI,
            kor.SPOL,
            kor.UPDATED,
            kor.CREATED,
            kor.GODISTE,
            kor.OIB,
            kor.CHGPASS,
            kor.DELETED,
            kor.KONTOLNI_BR_RAC,
            JSON_OBJECT(
                'MJESTO' value nas.mjesto,
                'POSTANSKIBROJ' value nas.postanskibroj,
                'OPCINA' value nas.opcina,
                'ZUPANIJA' value zup.zupanija
            )as NASELJA_JSON,
            JSON_OBJECT(
                'ADRESA' value kon.ADRESA,
                'KUCBRO' value kon.KUCBRO,
                'DODBRO' value kon.DODBRO,
                'MOBBRO' value kon.MOBBRO,
                'POZMOB' value kon.POZMOB
            )as KONTAKT_JSON,
            JSON_OBJECT(
            'BROJPARTIJE' value st.BROJPARTIJE,
            'VAZIDO' value st.VAZIDO,
            'IZNOSHRK' value st.IZNOSHRK,
            'IZNOSEUR' value st.IZNOSEUR
            )as STANJA_JSON
        from 
            korisnici kor, naselja nas, kontakti kon, stanja st, zupanije zup
        where
            kon.idnaselja = nas.id and
            nas.idzupanija = zup.id and 
            kon.idkorisnika = kor.id and
            st.idkorisnika = kor.id;
Begin
    FOR r_korisnik IN c_korisnici
    LOOP
        insert into J_KORISNICI
        values(
        r_korisnik.ID,
        r_korisnik.IME,
        r_korisnik.PREZIME,
        r_korisnik.EMAIL,
        r_korisnik.PASSWORD,
        r_korisnik.OVLASTI,
        r_korisnik.SPOL,
        r_korisnik.UPDATED,
        r_korisnik.CREATED,
        r_korisnik.GODISTE,
        r_korisnik.OIB,
        r_korisnik.CHGPASS,
        r_korisnik.DELETED,
        r_korisnik.KONTOLNI_BR_RAC,
        r_korisnik.NASELJA_JSON,
        r_korisnik.KONTAKT_JSON,
        r_korisnik.STANJA_JSON
        );
    END LOOP;
End;
/
--------------------------------------------------------------------------------------------------------------------------
--indexi
CREATE UNIQUE INDEX JSON_KOR_IDX_ID ON J_KORISNICI kor (kor.id);
CREATE INDEX JSON_KOR_IDX_STANJA ON J_KORISNICI kor (kor.STANJA_JSON.BROJPARTIJE);
/
--------------------------------------------------------------------------------------------------------------------------
--sekvenca
CREATE SEQUENCE  "FAKS"."SEQ_JSON_KOR_ID"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL;
/
--------------------------------------------------------------------------------------------------------------------------
--triggers
CREATE OR REPLACE NONEDITIONABLE TRIGGER "TRIG_JSON_KOR_BI" 
BEFORE INSERT ON J_KORISNICI
 FOR EACH ROW
  WHEN (new.ID IS NULL) BEGIN
 SELECT
 SEQ_JSON_KOR_ID.NEXTVAL
 INTO
 :new.ID
 FROM dual;

 SELECT
 SYSTIMESTAMP
 INTO
 :new.UPDATED
 FROM dual;

 SELECT
 SYSTIMESTAMP
 INTO
 :new.CREATED
 FROM dual;
END;
/
ALTER TRIGGER "TRIG_JSON_KOR_BI" ENABLE;
/
CREATE OR REPLACE NONEDITIONABLE TRIGGER "TRIG_JSON_KOR_AU" 
BEFORE UPDATE ON J_KORISNICI
REFERENCING NEW AS NEWER OLD AS
OLDER
FOR EACH ROW
BEGIN
 :NEWER.UPDATED := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "TRIG_JSON_KOR_AU" ENABLE;
/